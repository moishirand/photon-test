﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeNumber : MonoBehaviour {
    [SerializeField]
    private Text text;

    public float clients;

    private PhotonView photonView;

    // Use this for initialization
    void Start () {
        photonView = GetComponent<PhotonView>();
    }
	
	// Update is called once per frame
	void Update () {
        //if (photonView.isMine)
        //{
        //    InputTextChange();
        //}
        //InputTextChange();

        //text.text = clients.ToString();
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(clients);
            
        }
        else
        {
             clients = (float)stream.ReceiveNext();
           
        }

    }

    public void InputTextChange()
    {
        //if (Input.GetKeyDown(KeyCode.Space)|| Input.GetKeyDown(KeyCode.Mouse0))
        //{
        //    clients++;
        //    Change(clients);
        //}

        if (photonView.isMine)
        {
            clients++;
            Change(clients);
        }
       

    }

    [PunRPC]
    void Change(float num)
    {

        if (photonView.isMine)
        {
            // text.text = num.ToString();
            photonView.RPC("Change", PhotonTargets.OthersBuffered, num);

        }

        text.text = num.ToString();
    }
}
